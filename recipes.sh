#! /bin/bash

post_install() {
	xdg-icon-resource forceupdate --theme hicolor
	update-desktop-database --quiet
}

post_upgrade() {
	post_install
}

post_remove() {
	xdg-icon-resource forceupdate --theme hicolor
	update-desktop-database --quiet
}
