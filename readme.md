![screenshot.png](https://gitlab.com/es20490446e/vDisc/-/raw/main/info/screenshot.png)

```
USAGE
          ON THE FILE MANAGER
              Right click on an disc image file, and select "open with vDisc".

          ON THE TERMINAL
              vDisc [file]

EJECT
       If "file" is empty or already inserted, it is ejected instead.

STATUS
       If there is a disc inserted, "${HOME}/.config/vDisc/link" lists it.
